package com.mergexml;

public enum Flag {
    Modified("m"),
    New("n"),
    PictureNotFound("p"),
    Removed("r");

    private final String name;

    Flag(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
