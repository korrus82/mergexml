package com.mergexml;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;


class Test {

    public static void main(String[] args) throws XMLStreamException, IOException {
        File newXmlFile = new File(args[0]);
        File oldXmlFile = new File(args[1]);

        if (newXmlFile.exists() && oldXmlFile.exists()) {
            MergeXml mergeXml = new MergeXml(newXmlFile, oldXmlFile);
            mergeXml.merge();
        } else {
            System.out.println("not found file");
        }
    }
}
