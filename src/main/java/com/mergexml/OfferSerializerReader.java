package com.mergexml;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class OfferSerializerReader implements OfferReader {
    private RandomAccessFile randomAccessFile;
    private Map<Integer, Long> offerOffset;

    public OfferSerializerReader(File offerData, File offerMap) throws IOException {
        randomAccessFile = new RandomAccessFile(offerData, "r");
        offerOffset = new HashMap<>();
        try (DataInputStream dataInputStreamMap = new DataInputStream(new FileInputStream(offerMap))) {
            while (dataInputStreamMap.available() > 0) {
                offerOffset.put(dataInputStreamMap.readInt(), dataInputStreamMap.readLong());
            }
        }

    }

    @Override
    public Offer get(int id) throws IOException {
        Offer offer = null;
        Long seek = offerOffset.get(id);
        if (seek != null) {
            randomAccessFile.seek(seek);
            int size = randomAccessFile.readInt();
            byte[] array = new byte[size];
            randomAccessFile.read(array);
            try (ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(array))) {
                offer = (Offer) objectInputStream.readObject();
            } catch (ClassNotFoundException e) {
                throw new IOException(e);
            }
        }
        return offer;
    }

    @Override
    public Set<Integer> idSet() {
        return offerOffset.keySet();
    }

    @Override
    public void close() throws IOException {
        randomAccessFile.close();
    }
}
