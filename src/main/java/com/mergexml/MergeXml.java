package com.mergexml;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class MergeXml {
    private final File newXmlFile;
    private final File oldXmlFile;

    public MergeXml(File newXmlFile, File oldXmlFile) {
        this.newXmlFile = newXmlFile;
        this.oldXmlFile = oldXmlFile;
    }

    public void merge() throws IOException, XMLStreamException {

        File serFile = File.createTempFile("offer", "ser");
        File mapFile = File.createTempFile("offer", "map");
        serFile.deleteOnExit();
        mapFile.deleteOnExit();

        try (OfferXmlReader nextOldOffer = new OfferXmlReader(oldXmlFile);
             OfferWriter offerSerializerWriter = new OfferSerializerWriter(serFile, mapFile)) {
            Offer oldOffer;
            while ((oldOffer = nextOldOffer.readNextOffer()) != null) {

                offerSerializerWriter.put(oldOffer);
            }
        }

        ExecutorService executor = Executors.newFixedThreadPool(10);

        try (OfferXmlReader nextNewOffer = new OfferXmlReader(newXmlFile);
             OfferReader offerSerializerReader = new OfferSerializerReader(serFile, mapFile)) {
            Offer newOffer;
            Set<Integer> deletedIdSet = offerSerializerReader.idSet();
            while ((newOffer = nextNewOffer.readNextOffer()) != null) {
                Flag flag = Flag.New;
                Offer oldOffer = offerSerializerReader.get(newOffer.getId());
                if (oldOffer != null) {
                    deletedIdSet.remove(oldOffer.getId());
                    if (!oldOffer.equals(newOffer)) {
                        flag = Flag.Modified;
                    } else {
                        continue;
                    }
                }
                executor.execute(new CheckPictureUrl(newOffer.getId(), newOffer.getPicture(), flag));
            }
            for (Integer each : deletedIdSet) {
                System.out.println(each + " " + Flag.Removed);
            }
        }

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
