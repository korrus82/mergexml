package com.mergexml;

import java.io.*;

public class OfferSerializerWriter implements OfferWriter {
    private final ByteArrayOutputStream byteArrayOutputStream;
    private long offset;
    private FileOutputStream fileOutputStream;
    private FileOutputStream fileOutputStreamMap;
    private DataOutputStream dataOutputStream;
    private DataOutputStream dataOutputStreamMap;

    public OfferSerializerWriter(File offerData, File offerMap) throws IOException {

        offset = 0L;
        byteArrayOutputStream = new ByteArrayOutputStream();
        fileOutputStream = new FileOutputStream(offerData);
        fileOutputStreamMap = new FileOutputStream(offerMap);
        dataOutputStream = new DataOutputStream(fileOutputStream);
        dataOutputStreamMap = new DataOutputStream(fileOutputStreamMap);
    }

    @Override
    public void put(Offer offer) throws IOException {

        byteArrayOutputStream.reset();
        try (ObjectOutputStream out = new ObjectOutputStream(byteArrayOutputStream)) {
            out.writeObject(offer);
            dataOutputStream.writeInt(byteArrayOutputStream.size());
            byteArrayOutputStream.writeTo(dataOutputStream);

            dataOutputStreamMap.writeInt(offer.getId());
            dataOutputStreamMap.writeLong(offset);
            offset = offset + 4L + byteArrayOutputStream.size();
            //System.out.println("Serialize " + offer.getId() + " " + byteArrayOutputStream.size());
        }

    }

    @Override
    public void close() throws IOException {
        fileOutputStream.close();
        fileOutputStreamMap.close();
    }
}
