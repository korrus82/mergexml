package com.mergexml;

import java.io.Closeable;
import java.io.IOException;
import java.util.Set;

interface OfferReader extends Closeable {
    public Offer get(int id) throws IOException;

    public Set<Integer> idSet();
}
