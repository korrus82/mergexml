package com.mergexml;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.*;

class OfferXmlReader implements Closeable {

    private XMLStreamReader xmlStreamReader;
    private String currentText;
    private String currentElement;
    private Offer offer;
    private int eventType;

    public OfferXmlReader(File filename) {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            xmlStreamReader = factory.createXMLStreamReader(new FileInputStream(filename));
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        eventType = xmlStreamReader.getEventType();
    }

    public Offer readNextOffer() throws XMLStreamException {

        while (true) {

            eventType = xmlStreamReader.next();
            switch (eventType) {
                case XMLEvent.START_DOCUMENT:
                    break;
                case XMLEvent.CDATA:
                case XMLEvent.SPACE:
                case XMLEvent.CHARACTERS:
                    if (currentElement != null && currentText != null) {
                        currentText = xmlStreamReader.getText();
                    }
                    break;
                case XMLEvent.END_ELEMENT:
                    currentElement = xmlStreamReader.getLocalName().toUpperCase();
                    if (offer != null) {

                        switch (currentElement) {
                            case "URL":
                                offer.setUrl(currentText);
                                break;
                            case "PRICE":
                                offer.setPrice(Double.valueOf(currentText));
                                break;
                            case "CURRENCYID":
                                offer.setCurrencyId(currentText);
                                break;
                            case "CATEGORYID":
                                offer.setCategoryId(Integer.valueOf(currentText));
                                break;
                            case "PICTURE":
                                offer.setPicture(currentText);
                                break;
                            case "DELIVERY":
                                offer.setDelivery(Boolean.valueOf(currentText));
                                break;
                            case "NAME":
                                offer.setName(currentText);
                                break;
                            case "VENDOR":
                                offer.setVendor(currentText);
                                break;
                            case "DESCRIPTION":
                                offer.setDescription(currentText);
                                break;
                            case "OFFER":
                                eventType = XMLStreamConstants.START_ELEMENT;
                                return offer;
                        }
                        currentElement = null;
                        currentText = null;
                    }

                    break;
                case XMLEvent.START_ELEMENT:

                    currentElement = xmlStreamReader.getLocalName().toUpperCase();
                    currentText = "";

                    if (currentElement.equals("OFFER")) {
                        offer = new Offer();
                        offer.setId(Short.valueOf(xmlStreamReader.getAttributeValue(0)));
                        offer.setAvailable(Boolean.valueOf(xmlStreamReader.getAttributeValue(1)));
                    }
                    break;
            }
            if (!xmlStreamReader.hasNext()) {
                return null;
            }

        }
    }

    @Override
    public void close() throws IOException {
        try {
            xmlStreamReader.close();
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }
}