package com.mergexml;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

class CheckPictureUrl implements Runnable {
    private final int id;
    private final String url;
    private final Flag flag;

    public CheckPictureUrl(int id, String url, Flag flag) {
        this.id = id;
        this.url = url;
        this.flag = flag;
    }

    @Override
    public void run() {
        boolean result = false;
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");
            result = (connection.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (IOException ex) {
            result = false;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        if (result) {
            System.out.println(id + " " + flag);
        } else {
            System.out.println(id + " " + flag + Flag.PictureNotFound);
        }
    }
}
