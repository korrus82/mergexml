package com.mergexml;

import java.io.Closeable;
import java.io.IOException;

interface OfferWriter extends Closeable {
    public void put(Offer offer) throws IOException;
}

